# GitNex - Android client for Gitea

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

GitNex is an Android application for Gitea.
GitNex is licensed under the GPLv3 License. See the LICENSE file for the full license text.
No trackers are used and source code is available here for anyone to audit.

[WEBSITE](https://gitnex.com) [WIP]

[WIKI](https://gitlab.com/mmarif4u/gitnex/wikis/home)

[RELEASES](https://gitlab.com/mmarif4u/gitnex/tags)

## Contributing
Everyone can contribute to this project in many ways like writing code, creating layouts, designing the icons, suggesting acceptable changes, translating it to their native language etc. 

**Code contribution:**  
Fork the repository. Pull the forked repository from your namespace to your local machine. Create new branch and work on the bug/feature/enhacement etc. Push it to your forked version. From there create Merge Request(MR) against `develop` branch. 

**Translations:**  
Help us translate it to your native language. Ask if you are not sure how to start. :)

## Build from source
Option 1 - Open the project in Android Studio and build it there.

Option 2 - Open terminal(Linux) and cd to the project dir. Run `./gradlew build`.

## Features
1- Create new repository  
2- Create new organization  
3- List all the repositories  
4- List all the organizations  
5- Login  
More to come...

## Screenshots:
https://gitlab.com/mmarif4u/gitnex/wikis/Screenshots

## FAQ
https://gitlab.com/mmarif4u/gitnex/wikis/FAQ


Thanks to all the open source libraries.
- Retrofit
- Retrofit Converter Gson
- Gson
- Okhttp3

Follow me on Fediverse - https://mastodon.social/@mmarif
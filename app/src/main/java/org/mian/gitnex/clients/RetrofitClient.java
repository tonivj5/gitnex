package org.mian.gitnex.clients;

import android.util.Log;
import org.mian.gitnex.interfaces.ApiInterface;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Author M M Arif
 */

public class RetrofitClient {

    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient(String instanceUrl) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        okhttp3.Response response = chain.proceed(request);

                        if (response.code() == 400 || response.code() == 500 || response.code() == 502) {
                            Log.i("Error-code", String.valueOf(response.code()));
                            return response;
                        }
                        return response;
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(instanceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

    }

    public static synchronized RetrofitClient getInstance(String instanceUrl) {
        if (mInstance == null) {
            mInstance = new RetrofitClient(instanceUrl);
        }
        return mInstance;
    }

    public ApiInterface getApiInterface() {
        return retrofit.create(ApiInterface.class);
    }

}

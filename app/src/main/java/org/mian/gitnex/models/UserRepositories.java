package org.mian.gitnex.models;

import com.google.gson.annotations.SerializedName;

/**
 * Author M M Arif
 */

public class UserRepositories {

    private int id;
    private String name;
    private String full_name;
    private String description;
    @SerializedName("private")
    private boolean privateFlag;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullname() {
        return full_name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getPrivateFlag() {
        return privateFlag;
    }

}

package org.mian.gitnex.adaptors;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import org.mian.gitnex.R;
import org.mian.gitnex.models.UserOrganizations;
import org.mian.gitnex.util.RoundedTransformation;
import java.util.List;

/**
 * Author M M Arif
 */

public class OrganizationsListAdaptor extends RecyclerView.Adapter<OrganizationsListAdaptor.OrganizationsViewHolder> {

    private List<UserOrganizations> orgsList;
    private Context mCtx;

    static class OrganizationsViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView mTextView1;
        private TextView mTextView2;

        private OrganizationsViewHolder(View itemView) {
            super(itemView);
            mTextView1 = itemView.findViewById(R.id.orgUsername);
            mTextView2 = itemView.findViewById(R.id.orgDescription);
            image = itemView.findViewById(R.id.imageAvatar);
        }
    }

    public OrganizationsListAdaptor(Context mCtx, List<UserOrganizations> orgsListMain) {
        this.mCtx = mCtx;
        this.orgsList = orgsListMain;
    }

    @NonNull
    @Override
    public OrganizationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.organizations_list, parent, false);
        return new OrganizationsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationsViewHolder holder, int position) {


        UserOrganizations currentItem = orgsList.get(position);
        holder.mTextView2.setVisibility(View.GONE);

        Picasso.get().load(currentItem.getAvatar_url()).transform(new RoundedTransformation(100, 0)).resize(200, 200).centerCrop().into(holder.image);
        holder.mTextView1.setText(currentItem.getUsername());
        if (!currentItem.getDescription().equals("")) {
            holder.mTextView2.setVisibility(View.VISIBLE);
            holder.mTextView2.setText(currentItem.getDescription());
        }

    }

    @Override
    public int getItemCount() {
        return orgsList.size();
    }


}

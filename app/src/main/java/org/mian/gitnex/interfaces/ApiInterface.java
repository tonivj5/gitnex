package org.mian.gitnex.interfaces;

import org.mian.gitnex.models.GiteaVersion;
import org.mian.gitnex.models.OrgOwner;
import org.mian.gitnex.models.OrganizationRepository;
import org.mian.gitnex.models.UserInfo;
import org.mian.gitnex.models.UserOrganizations;
import org.mian.gitnex.models.UserRepositories;
import org.mian.gitnex.models.UserTokens;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Author M M Arif
 */

public interface ApiInterface {

    @GET("version") // gitea version API
    Call<GiteaVersion> getGiteaVersion();

    @GET("user") // username, full name, email
    Call<UserInfo> getUserInfo(@Header("Authorization") String token);

    @GET("users/{username}/tokens") // get user token
    Call<List<UserTokens>> getUserTokens(@Header("Authorization") String authorization, @Path("username") String loginUid);

    @POST("users/{username}/tokens") // create new token
    Call<UserTokens> createNewToken(@Header("Authorization") String authorization, @Path("username") String loginUid, @Body UserTokens jsonStr);

    @GET("user/orgs") // get user organizations
    Call<List<UserOrganizations>> getUserOrgs(@Header("Authorization") String token);

    @POST("orgs") // create new organizations
    Call<UserOrganizations> createNeworganization(@Header("Authorization") String token, @Body UserOrganizations jsonStr);

    @POST("org/{org}/repos") // create new repository under org
    Call<OrganizationRepository> createNewUserOrgRepository(@Header("Authorization") String token, @Path("org") String orgName, @Body OrganizationRepository jsonStr);

    @GET("user/orgs") // get user organizations
    Call<List<OrgOwner>> getOrgOwners(@Header("Authorization") String token);

    @GET("user/repos") // get user repositories
    Call<List<UserRepositories>> getUserRepositories(@Header("Authorization") String token);

    @POST("user/repos") // get user repositories
    Call<OrganizationRepository> createNewUserRepository(@Header("Authorization") String token, @Body OrganizationRepository jsonStr);

}

package org.mian.gitnex.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tooltip.Tooltip;
import org.mian.gitnex.clients.RetrofitClient;
import org.mian.gitnex.models.UserTokens;
import org.mian.gitnex.util.AppUtil;
import org.mian.gitnex.R;
import org.mian.gitnex.util.TinyDB;
import java.util.List;
import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Author M M Arif
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button login_button;
    private EditText instance_url, login_uid, login_passwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TinyDB tinyDb = new TinyDB(getApplicationContext());

        login_button = findViewById(R.id.login_button);
        instance_url = findViewById(R.id.instance_url);
        login_uid = findViewById(R.id.login_uid);
        login_passwd = findViewById(R.id.login_passwd);
        ImageView info_button = findViewById(R.id.info);
        final TextView viewTextGiteaVersion = findViewById(R.id.appVersion);

        viewTextGiteaVersion.setText(AppUtil.getAppVersion(getApplicationContext()));

        //login_button.setOnClickListener(this);

        if(tinyDb.getBoolean("loggedInMode")) {

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();

        }

        login_button.setOnClickListener(loginListener);

        info_button.setOnClickListener(infoListener);

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.login_button:
                login();
                break;
            default:

        }

    }

    private View.OnClickListener loginListener = new View.OnClickListener() {
        public void onClick(View v) {
        login_button.setEnabled(false);
        login_button.setText(R.string.processingText);
        login();
        }
    };

    private View.OnClickListener infoListener = new View.OnClickListener() {
        public void onClick(View v) {
        new Tooltip.Builder(v)
            .setText(R.string.urlInfoTooltip)
            .setTextColor(getResources().getColor(R.color.white))
            .setBackgroundColor(getResources().getColor(R.color.tooltipBackground))
            .setCancelable(true)
            .setDismissOnClick(true)
            .setPadding(30)
            .setCornerRadius(R.dimen.tooltipCornor)
            .setGravity(Gravity.BOTTOM)
            .show();
        }
    };

    @SuppressLint("ResourceAsColor")
    private void login() {

        TinyDB tinyDb = new TinyDB(getApplicationContext());
        AppUtil appUtil = new AppUtil();
        boolean connToInternet = AppUtil.haveNetworkConnection(LoginActivity.this);

        tinyDb.remove("loginUid");
        tinyDb.remove("instanceUrl");
        tinyDb.remove("loginPass");

        String instanceUrl = instance_url.getText().toString();
        String loginUid = login_uid.getText().toString();
        String loginPass = login_passwd.getText().toString();

        if (!instanceUrl.startsWith("http://") || !instanceUrl.startsWith("https://")) {
            instanceUrl = "https://" + instanceUrl + "/api/v1/";
        }
        else {
            instanceUrl = instanceUrl + "/api/v1/";
        }

        tinyDb.putString("loginUid", loginUid);
        tinyDb.putString("instanceUrl", instanceUrl);

        //Log.i("TinyDB old Token: ", tinyDb.getString("loginUid" + "-token"));

        if(connToInternet) {

            if(instance_url.getText().toString().equals("") || login_uid.getText().toString().equals("") ||
                    login_passwd.getText().toString().equals("")) {

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.custom_toast_container));

                TextView text = layout.findViewById(R.id.toastText);
                text.setText(R.string.emptyFields);

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
                login_button.setEnabled(true);
                login_button.setText(R.string.btnLogin);
                return;

            }

            //Log.i("isConnectedToInternet", "Connected");
            letTheUserIn(instanceUrl, loginUid, loginPass);

        }
        else {

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast,
                    (ViewGroup) findViewById(R.id.custom_toast_container));

            TextView text = layout.findViewById(R.id.toastText);
            text.setText(R.string.checkNetConnection);

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();

        }

    }

    private void letTheUserIn(final String instanceUrl, final String loginUid, final String loginPass) {

        final String credential = Credentials.basic(loginUid, loginPass);

        Call<List<UserTokens>> call = RetrofitClient
                .getInstance(instanceUrl)
                .getApiInterface()
                .getUserTokens(credential, loginUid);

        call.enqueue(new Callback<List<UserTokens>>() {

            @Override
            public void onResponse(@NonNull Call<List<UserTokens>> call, @NonNull retrofit2.Response<List<UserTokens>> response) {

                List<UserTokens> userTokens = response.body();
                final TinyDB tinyDb = new TinyDB(getApplicationContext());

                if (response.isSuccessful()) {

                    if (response.code() == 200) {

                        boolean setTokenFlag = false;

                        assert userTokens != null;
                        if (userTokens.size() > 0) {
                            for (int i = 0; i < userTokens.size(); i++) {
                                if (userTokens.get(i).getSha1().equals(tinyDb.getString(loginUid + "-token"))) {
                                    setTokenFlag = true;
                                    break;
                                }
                                //Log.i("Tokens: ", userTokens.get(i).getSha1());
                            }
                        }

                        if(tinyDb.getString(loginUid + "-token").isEmpty() || !setTokenFlag) {

                            UserTokens createUserToken = new UserTokens("gitnex-app-token");

                            Call<UserTokens> callCreateToken = RetrofitClient
                                .getInstance(instanceUrl)
                                .getApiInterface()
                                .createNewToken(credential, loginUid, createUserToken);

                            callCreateToken.enqueue(new Callback<UserTokens>() {

                                @Override
                                public void onResponse(@NonNull Call<UserTokens> callCreateToken, @NonNull retrofit2.Response<UserTokens> responseCreate) {

                                    if (responseCreate.isSuccessful()) {

                                        if(responseCreate.code() == 201) {

                                            UserTokens newToken = responseCreate.body();
                                            assert newToken != null;
                                            //Log.i("Tokens-NEW", "new:" + newToken.getSha1());

                                            if (!newToken.getSha1().equals("")) {

                                                tinyDb.remove("loginPass");
                                                tinyDb.putBoolean("loggedInMode", true);
                                                tinyDb.putString(loginUid + "-token", newToken.getSha1());
                                                //Log.i("Tokens", "new:" + newToken.getSha1() + " old:" + tinyDb.getString(loginUid + "-token"));

                                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                finish();

                                            }

                                        }

                                    }

                                }

                                @Override
                                public void onFailure(@NonNull Call<UserTokens> createUserToken, Throwable t) {

                                }

                            });
                        }
                        else {

                            //Log.i("Current Token", tinyDb.getString(loginUid + "-token"));
                            tinyDb.putBoolean("loggedInMode", true);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        }

                    }

                }
                else {

                    String toastError = getResources().getString(R.string.genericApiStatusError) + String.valueOf(response.code());
                    Log.i("error message else4", String.valueOf(response.code()));
                    LayoutInflater inflater1 = getLayoutInflater();
                    View layout = inflater1.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.custom_toast_container));

                    TextView text = layout.findViewById(R.id.toastText);
                    text.setText(toastError);

                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                    login_button.setEnabled(true);
                    login_button.setText(R.string.btnLogin);

                }

            }

            @Override
            public void onFailure(@NonNull Call<List<UserTokens>> call, @NonNull Throwable t) {
                Log.e("onFailure", t.toString());
                login_button.setEnabled(true);
                login_button.setText(R.string.btnLogin);
            }
        });

    }

}

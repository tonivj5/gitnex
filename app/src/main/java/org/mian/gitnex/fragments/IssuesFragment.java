package org.mian.gitnex.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.mian.gitnex.R;
import org.mian.gitnex.activities.MainActivity;
import java.util.Objects;

/**
 * Author M M Arif
 */

public class IssuesFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((MainActivity) Objects.requireNonNull(getActivity())).setActionBarTitle(getResources().getString(R.string.pageTitleIssues));
        return inflater.inflate(R.layout.fragment_issues, container, false);
    }
}

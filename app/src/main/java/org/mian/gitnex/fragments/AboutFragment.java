package org.mian.gitnex.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import org.mian.gitnex.R;
import org.mian.gitnex.activities.MainActivity;
import org.mian.gitnex.clients.RetrofitClient;
import org.mian.gitnex.models.GiteaVersion;
import org.mian.gitnex.util.AppUtil;
import org.mian.gitnex.util.TinyDB;
import java.util.Objects;
import retrofit2.Response;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Author M M Arif
 */

public class AboutFragment extends Fragment {

    private TextView viewTextGiteaVersion;
    private ProgressBar mProgressBar;
    private LinearLayout pageContent;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_about, container, false);
        ((MainActivity) Objects.requireNonNull(getActivity())).setActionBarTitle(getResources().getString(R.string.pageTitleAbout));

        TinyDB tinyDb = new TinyDB(getContext());
        String instanceUrl = tinyDb.getString("instanceUrl");

        final TextView viewTextVersion;
        final TextView viewTextBuild;
        final Button donationLink;

        pageContent = v.findViewById(R.id.aboutFrame);
        pageContent.setVisibility(View.GONE);

        mProgressBar = v.findViewById(R.id.progress_bar);

        viewTextVersion = v.findViewById(R.id.appVersion);
        viewTextBuild = v.findViewById(R.id.appBuild);
        viewTextGiteaVersion = v.findViewById(R.id.giteaVersion);

        donationLink = v.findViewById(R.id.donationLink);

        //Log.i("App Version", AppUtil.getAppVersion(getContext()));
        String version = getResources().getString(R.string.appVersion) + AppUtil.getAppVersion(Objects.requireNonNull(getContext()));
        String build = getResources().getString(R.string.appBuild) + AppUtil.getAppBuildNo(getContext());
        viewTextVersion.setText(version);
        viewTextBuild.setText(build);

        donationLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getResources().getString(R.string.supportLink)));
                startActivity(intent);
            }
        });

        boolean connToInternet = AppUtil.haveNetworkConnection(getContext());

        if(!connToInternet) {

            LayoutInflater inflater1 = getLayoutInflater();
            View layout = inflater1.inflate(R.layout.custom_toast,
                    (ViewGroup) v.findViewById(R.id.custom_toast_container));

            TextView text = layout.findViewById(R.id.toastText);
            text.setText(R.string.checkNetConnection);

            Toast toast = new Toast(getContext());
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
            mProgressBar.setVisibility(View.GONE);
            pageContent.setVisibility(View.VISIBLE);

        } else {

            giteaVer(instanceUrl);

        }

        return v;
    }

    private void giteaVer(String instanceUrl) {

        Call<GiteaVersion> call = RetrofitClient
                .getInstance(instanceUrl)
                .getApiInterface()
                .getGiteaVersion();

        call.enqueue(new Callback<GiteaVersion>() {

            @Override
            public void onResponse(@NonNull Call<GiteaVersion> call, @NonNull Response<GiteaVersion> response) {

                if (response.isSuccessful()) {
                    if (response.code() == 200) {

                        GiteaVersion version = response.body();
                        assert version != null;
                        String commit = getResources().getString(R.string.commitPage) + version.getVersion();
                        viewTextGiteaVersion.setText(commit);
                        mProgressBar.setVisibility(View.GONE);
                        pageContent.setVisibility(View.VISIBLE);

                    }

                }
                else {

                    String commit = getResources().getString(R.string.commitPage);
                    viewTextGiteaVersion.setText(commit);
                    mProgressBar.setVisibility(View.GONE);
                    pageContent.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GiteaVersion> call, @NonNull Throwable t) {
                Log.e("onFailure", t.toString());
            }
        });

    }

}

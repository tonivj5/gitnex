# Contributing

Everyone can contribute to this project in many ways like writing code, creating layouts, designing the icons, suggesting acceptable changes, translating it to their native language etc. 

**Code contributaion:**  
Fork the repository. Pull the forked repository from your namespace to your local machine. Create new branch and work on the bug/feature/enhacement etc. Push it to your forked version. From there create Merge Request(MR) against `develop` branch. 

**Translations:**  
Help us translate it to your native language. Ask if you are not sure how to start. :)